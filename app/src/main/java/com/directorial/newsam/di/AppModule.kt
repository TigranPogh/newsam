package com.directorial.newsam

import android.app.Application
import androidx.room.Room
import com.directorial.newsam.db.AppDatabase
import com.directorial.newsam.db.dao.NewsDAO
import com.directorial.newsam.viewmodel.NewsViewModel
import com.directorial.newsam.viewmodel.repo.FakeNewsRepositoryImpl
import com.directorial.newsam.viewmodel.repo.NewsRepository
import com.directorial.newsam.viewmodel.repo.NewsRepositoryImpl
import com.prof.rssparser.Parser
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.qualifier.named
import org.koin.dsl.module

private fun provideDatabase(application: Application): AppDatabase {
    return Room.databaseBuilder(application, AppDatabase::class.java, "database").build()
}

private fun provideDao(appDatabase: AppDatabase): NewsDAO? {
    return appDatabase.newsDAO()
}

private fun provideParser(application: Application): Parser {
    return Parser.Builder()
        .context(application)
        // If you want to provide a custom charset (the default is utf-8):
        // .charset(Charset.forName("ISO-8859-7"))
        .cacheExpirationMillis(24L * 60L * 60L * 100L) // one day
        .build()
}

val parserModule = module {
    single { provideParser(androidApplication()) }
}

val dbModule = module {
    single { provideDatabase(androidApplication()) }
    single { provideDao(get()) }
}

val repoModule = module {
    single<NewsRepository>(qualifier = named(REAL_REPO_NAME)) {
        return@single NewsRepositoryImpl(get())
    }

    single<NewsRepository>(qualifier = named(FAKE_REPO_NAME)) {
        return@single FakeNewsRepositoryImpl(get())
    }
}

val viewModelModule = module {

    viewModel(qualifier = named(REAL_VIEW_MODEL_NAME)) {
        NewsViewModel(get(qualifier = named(REAL_REPO_NAME)))
    }

    viewModel(qualifier = named(FAKE_VIEW_MODEL_NAME)) {
        NewsViewModel(get(qualifier = named(FAKE_REPO_NAME)))
    }
}

