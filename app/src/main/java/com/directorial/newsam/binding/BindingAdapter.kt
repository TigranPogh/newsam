package com.directorial.newsam.binding

import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.text.SimpleDateFormat
import java.util.*

class BindingAdapter {

    companion object {
        @JvmStatic
        @BindingAdapter("date")
        fun setDate(textView: TextView, date: String?) {
            try {
                val sourceSdf = SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z", Locale.ENGLISH)
                if (date != null) {
                    val sdfDate = sourceSdf.parse(date)
                    if (sdfDate != null) {
                        val sdf = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
                        textView.text = sdf.format(sdfDate)
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}