package com.directorial.newsam

const val BASE_URL = "https://news.am/arm/rss/"
const val REAL_REPO_NAME = "real"
const val FAKE_REPO_NAME = "fake"
const val FAKE_VIEW_MODEL_NAME = "view_real"
const val REAL_VIEW_MODEL_NAME = "view_fake"
const val REQUEST_REFRESH_TIME = 60000L