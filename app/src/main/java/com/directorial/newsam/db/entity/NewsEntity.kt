package com.directorial.newsam.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "dao_model")
data class NewsEntity(
        @PrimaryKey
        @ColumnInfo(name = "title") val title: String,
        @ColumnInfo(name = "date") val date: String
        
)