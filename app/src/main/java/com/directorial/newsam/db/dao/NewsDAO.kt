package com.directorial.newsam.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.directorial.newsam.db.entity.NewsEntity

@Dao
interface NewsDAO {
    @get:Query("SELECT * FROM dao_model")
    val all: LiveData<List<NewsEntity>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(newsEntity: NewsEntity?)
}