package com.directorial.newsam.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.directorial.newsam.db.dao.NewsDAO
import com.directorial.newsam.db.entity.NewsEntity

@Database(entities = [NewsEntity::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun newsDAO(): NewsDAO?
}