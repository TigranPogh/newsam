package com.directorial.newsam.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.directorial.newsam.databinding.SavedArticleRowBinding
import com.directorial.newsam.db.entity.NewsEntity

class SavedArticleAdapter : RecyclerView.Adapter<SavedArticleAdapter.ViewHolder>() {

    private lateinit var list: List<NewsEntity>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding =
            SavedArticleRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(list[position])

    override fun getItemCount() = list.size

    fun setItems(articles: List<NewsEntity>) {
        this.list = articles
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: SavedArticleRowBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(newsEntity: NewsEntity) {
            binding.article = newsEntity
        }
    }
}