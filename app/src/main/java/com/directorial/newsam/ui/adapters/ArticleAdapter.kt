package com.directorial.newsam.ui.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.directorial.newsam.R
import com.directorial.newsam.databinding.ArticleRowBinding
import com.directorial.newsam.db.entity.NewsEntity
import com.prof.rssparser.Article
import kotlinx.android.synthetic.main.article_row.view.*
import java.text.SimpleDateFormat
import java.util.*

class ArticleAdapter(private val itemClick: ((NewsEntity) -> Unit)? = null) :
    RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {

    private lateinit var articles: MutableList<Article>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ArticleRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(articles[position], itemClick ?: { print("Clicked") })

    override fun getItemCount() = articles.size

    fun setItems(articles: MutableList<Article>) {
        this.articles = articles
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ArticleRowBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(article: Article, itemClick: ((NewsEntity) -> Unit)) {

            binding.article = article

            val mainModel = NewsEntity(article.title!!, article.pubDate!!)
            itemView.btnSave.setOnClickListener { itemClick(mainModel) }
        }
    }
}