package com.directorial.newsam.ui.activities

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.directorial.newsam.FAKE_VIEW_MODEL_NAME
import com.directorial.newsam.REAL_VIEW_MODEL_NAME
import com.directorial.newsam.REQUEST_REFRESH_TIME
import com.directorial.newsam.databinding.ActivityNewsBinding
import com.directorial.newsam.ui.adapters.ArticleAdapter
import com.directorial.newsam.ui.adapters.SavedArticleAdapter
import com.directorial.newsam.viewmodel.NewsViewModel
import com.prof.rssparser.Parser
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.getViewModel
import org.koin.core.qualifier.named

class NewsActivity : AppCompatActivity() {

    // binding
    private lateinit var binding: ActivityNewsBinding

    // ViewModel
    private lateinit var viewModel: NewsViewModel

    // Handler
    private var handler: Handler? = null

    // Parser
    private val parser: Parser by inject()

    private val articleAdapter: ArticleAdapter by lazy {
        val adapter = ArticleAdapter {
            viewModel.saveNews(it)
            Toast.makeText(this, "Saved to favorites list", Toast.LENGTH_SHORT).show()
        }
        binding.newsRecyclerView.adapter = adapter
        return@lazy adapter
    }

    private val savedArticlesAdapter: SavedArticleAdapter by lazy {
        val adapter = SavedArticleAdapter()
        binding.savedNewsRecyclerView.adapter = adapter
        return@lazy adapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNewsBinding.inflate(layoutInflater, null, false)
        setContentView(binding.root)

        viewModel = getViewModel(qualifier = named(REAL_VIEW_MODEL_NAME))

        initObservers()

        binding.toggleButton.setOnCheckedChangeListener { _, isChecked ->
            viewModel = if (isChecked) {
                getViewModel(qualifier = named(FAKE_VIEW_MODEL_NAME))
            } else {
                getViewModel(qualifier = named(REAL_VIEW_MODEL_NAME))
            }
            initNews()
        }

        handler = Handler(Looper.getMainLooper())
        handler?.post(object : Runnable {
            override fun run() {
                initNews()
                handler?.postDelayed(this, REQUEST_REFRESH_TIME)
            }
        })
    }

    private fun initObservers() {
        viewModel.getSavedNewsLiveData().observe(this, Observer {
            savedArticlesAdapter.setItems(it)
        })
    }

    private fun initNews() {
        viewModel.fetchFeed(parser)

        viewModel.rssChannel.observe(this, { channel ->
            if (channel != null) {
                if (channel.title != null) {
                    title = channel.title
                }
                articleAdapter.setItems(channel.articles)
                binding.progressBar.visibility = View.GONE
            }
        })
    }

    override fun onDestroy() {
        handler?.let {
            it.removeCallbacksAndMessages(null)
            handler = null
        }
        super.onDestroy()
    }
}