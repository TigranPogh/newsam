package com.directorial.newsam.viewmodel.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.directorial.newsam.BASE_URL
import com.directorial.newsam.db.dao.NewsDAO
import com.directorial.newsam.db.entity.NewsEntity
import com.prof.rssparser.Channel
import com.prof.rssparser.Parser

class NewsRepositoryImpl(private val newsDAO: NewsDAO) : NewsRepository {

    override suspend fun getAllData(parser: Parser, _rssChannel: MutableLiveData<Channel>) {
        try {
            val channel = parser.getChannel(BASE_URL)
            _rssChannel.postValue(channel)
        } catch (e: Exception) {
            e.printStackTrace()
            _rssChannel.postValue(Channel(null, null, null, null, null, null, mutableListOf()))
        }
    }

    override suspend fun saveNews(newsEntity: NewsEntity) {
        newsDAO.insert(newsEntity)
    }

    override fun getSavedNews(): LiveData<List<NewsEntity>> = newsDAO.all
}