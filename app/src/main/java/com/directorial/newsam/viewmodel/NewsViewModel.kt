package com.directorial.newsam.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.directorial.newsam.viewmodel.repo.NewsRepository
import com.directorial.newsam.db.entity.NewsEntity
import com.prof.rssparser.Channel
import com.prof.rssparser.Parser
import kotlinx.coroutines.launch

class NewsViewModel(private val repository : NewsRepository) : ViewModel() {

    private val _rssChannel = MutableLiveData<Channel>()
    val rssChannel: LiveData<Channel>
        get() = _rssChannel

    fun fetchFeed(parser: Parser) {
        viewModelScope.launch {
            repository.getAllData(parser, _rssChannel)
        }
    }
    fun saveNews(newsEntity: NewsEntity) = viewModelScope.launch {
        repository.saveNews(newsEntity)
    }

    fun getSavedNewsLiveData() : LiveData<List<NewsEntity>> = repository.getSavedNews()
}