package com.directorial.newsam.viewmodel.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.directorial.newsam.db.entity.NewsEntity
import com.prof.rssparser.Channel
import com.prof.rssparser.Parser

interface NewsRepository {

    suspend fun getAllData(parser: Parser, _rssChannel: MutableLiveData<Channel>)

    suspend fun saveNews(newsEntity: NewsEntity)

    fun getSavedNews(): LiveData<List<NewsEntity>>
}