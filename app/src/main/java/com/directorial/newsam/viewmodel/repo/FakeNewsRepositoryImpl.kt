package com.directorial.newsam.viewmodel.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.directorial.newsam.db.dao.NewsDAO
import com.directorial.newsam.db.entity.NewsEntity
import com.prof.rssparser.Article
import com.prof.rssparser.Channel
import com.prof.rssparser.Parser

class FakeNewsRepositoryImpl(private val newsDAO: NewsDAO) : NewsRepository {

    private val list = mutableListOf<Article>()

    init {
        list.add(Article(null, "menu.am-ը հասանելի է արդեն ԱՄՆ-ի Գլենդել համայնքի քաղաքացիների համար", null, null, "20 Dec 2020", null, null, null, null, null, null, null, mutableListOf()))
        list.add(Article(null, "menu.am-ը հասանելի է արդեն Ռուսաստանի քաղաքացիների համար", null, null, "22 Dec 2020", null, null, null, null, null, null, null, mutableListOf()))
        list.add(Article(null, "menu.am-ը հասանելի է արդեն Հունաստանի քաղաքացիների համար", null, null, "22 Dec 2020", null, null, null, null, null, null, null, mutableListOf()))
        list.add(Article(null, "menu.am-ը հասանելի է արդեն Գերմանիայի քաղաքացիների համար", null, null, "25 Dec 2020", null, null, null, null, null, null, null, mutableListOf()))
        list.add(Article(null, "menu.am-ը հասանելի է արդեն Վրաստանի քաղաքացիների համար", null, null, "25 Dec 2020", null, null, null, null, null, null, null, mutableListOf()))
        list.add(Article(null, "menu.am-ը հասանելի է արդեն Ուկրաինայի քաղաքացիների համար", null, null, "25 Dec 2020", null, null, null, null, null, null, null, mutableListOf()))
        list.add(Article(null, "menu.am-ը հասանելի է արդեն Իտալիայի քաղաքացիների համար", null, null, "25 Dec 2020", null, null, null, null, null, null, null, mutableListOf()))
        list.add(Article(null, "menu.am-ը հասանելի է արդեն Իսպանիայի քաղաքացիների համար", null, null, "27 Dec 2020", null, null, null, null, null, null, null, mutableListOf()))
    }

    override suspend fun getAllData(parser: Parser, _rssChannel: MutableLiveData<Channel>) {
        _rssChannel.postValue(Channel(null, null, null, null, null, null, list))
    }

    override suspend fun saveNews(newsEntity: NewsEntity) {
        newsDAO.insert(newsEntity)
    }

    override fun getSavedNews(): LiveData<List<NewsEntity>> = newsDAO.all
}